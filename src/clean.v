import os

fn clean_input_data(input_file string, output_file string, min_sentence_length u8) ! {
	mut lines := []string{}

	for raw_line in os.read_lines(input_file)! {
		mut line := raw_line.trim(' +')

		if line == '' { continue }
		if line == ' ' { continue }
		if line.starts_with('\n') { continue }
		if line.contains('  ') { continue }
		if line.split(' ').len < min_sentence_length { continue }

		line = line.replace('@', '@​')

		lines << line
	}

	os.write_file(output_file, lines.join('\n'))!
}
