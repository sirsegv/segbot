import os
import generators
import outputs

// Runs through a config file and processes all lines
fn (mut p Program) run_commands(command_file string) ! {
	commands := os.read_lines(command_file) or {
		eprintln('Unable to read command file: ${err.msg()}')
		exit(1)
	}

	for command_str in commands {
		if command_str.starts_with('#') || command_str.starts_with('//') { continue }

		// Array of the current command and its arguments
		command := command_str.split(' ')

		// Process command
		match command[0] {
			'name' {
				p.config_name = command[1] or {
					return error('Command "name" requires a string')
				}
			}
			'load' {
				file := command[1] or {
					return error('Command "load" requires a filename')
				}
				p.input.files << file
			}
			'generate' {
				generator := command[1] or {
					return error('Command "generate" requires a mode')
				}.to_lower()
				match generator {
					'quote' {
						p.generator = generators.QuoteGenerator{}
					}
					'markov' {
						p.generator = generators.MarkovGenerator{n_order: command[2] or {'2'}.u8()}
					}
					else {
						return error('Unknown generator: "${generator}"')
					}
				}
			}
			'output' {
				output := command[1] or {
					return error('Command "output" requires an output mode')
				}.to_lower()
				match output {
					'print' {
						p.outputs << outputs.Print{}
					}
					'misskey' {
						instance := command[2] or { return error('Output "misskey" requires an instance url') }
						api_key := command[3] or { return error('Output "misskey" requires an api key') }
						p.outputs << outputs.Misskey{instance: instance, api_key: api_key}
					}
					'mastodon' {
						instance := command[2] or { return error('Output "mastodon" requires an instance url') }
						api_key := command[3] or { return error('Output "mastodon" requires an api key') }
						p.outputs << outputs.Mastodon{instance: instance, api_key: api_key}
					}
					else {
						return error('Unknown output: "${output}"')
					}
				}
			}
			'delay' {
				p.post_frequency = command[1] or { '60' }.f32()
			}
			'skip_wait' {
				p.opts.skip_wait = true
			}
			'unique' {
				p.opts.unique = true
			}
			'cw' {
				p.cw = command[1..].join(' ')
			}
			'' {}
			else {
				println('Unknown command: ${command[0]}')
			}
		}
	}
}