module inputdata

import os
import time

// minimum amount of delay time for the data to be unloaded
const min_unload_delay = 2

pub struct InputData {
pub:
	quiet bool
pub mut:
	files []string
	data []string
	loaded bool
}

pub fn (mut i InputData) get() []string {
	if !i.loaded {
		i.load()
	}
	
	return i.data
}

pub fn (mut i InputData) contains(text string) bool {
	return i.get().contains(text)
}

pub fn (mut i InputData) load() {
	for file in i.files {
		lines := os.read_lines(file) or {
			if !i.quiet { eprintln('Unable to read input file: ${err.msg()}') }
			continue
		}
		
		i.data << lines
	}

	i.loaded = true
}

pub fn (mut i InputData) add(line string) {
	i.data << line
}

// unloads the data if time is big enough
pub fn (mut i InputData) sleep(delay i64) {
	if delay >= (min_unload_delay * time.minute) {
		i.unload()
	}
}

pub fn (mut i InputData) unload() {
	unsafe{i.data.free()}
	i.data = []
	i.loaded = false
}
