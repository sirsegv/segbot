import time
import os

const data_filename = 'data'
const history_filename = 'history'

struct SaveData {
mut:
	last_post time.Time
	last_reply_check time.Time
}

fn (sd SaveData) save_data(file string) {
	save_string := sd.last_post.format_rfc3339() + '\n'
		+ sd.last_reply_check.format_rfc3339()
	
	os.write_file(file, save_string) or {
		eprintln('Error saving data: ${err.msg()}')
	}
}

fn (mut sd SaveData) load_data(file string) ! {
	filedata := os.read_file(file) or {
		eprintln('Error loading data: ${err.msg()}')
		return
	}.split('\n')

	sd.last_post = time.parse_rfc3339(filedata[0] or {return}) or { eprintln('Error parsing last post: ${err.msg()}') time.utc() }
	sd.last_reply_check = time.parse_rfc3339(filedata[1] or {return}) or { eprintln('Error parsing last reply check: ${err.msg()}') time.utc() }
}

// Returns a save file location.
// Creates the file if it doesn't exist.
fn (p Program) data_file(filename string) string {
	data_dir := p.config_name + '_botdata'
	if !os.exists(data_dir) {
		println('Creating data directory: ${data_dir}')
		os.mkdir_all(data_dir) or {
			eprintln('Unable to create config folder: ${err.msg()}')
			exit(1)
		}
	}
	
	data_file := os.join_path(data_dir, filename)
	if !os.exists(data_file) {
		os.create(data_file) or {
			eprintln('Unable to create data file: ${err.msg()}')
			exit(1)
		}
	}

	return data_file
}

// Adds a line to the bots post history
fn (mut p Program) add_history(line string) {
	p.history.add(line)
	
	mut history := os.open_append(p.data_file(history_filename)) or {
		eprintln('Unable to write to history: ${err.msg()}')
		return
	}

	defer { history.close() }

	history.write_string(line + '\n') or {
		eprintln('Unable to write to history: ${err.msg()}')
		return
	}
}
