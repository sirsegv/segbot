import os
import flag

struct Opts {
mut:
	file string

	unique bool
	skip_wait bool

	clean ?CleanOpts
}

struct CleanOpts {
mut:
	input string
	output string

	min_length u8
}

// Handles CLI args
fn parse_flags() Opts {
	mut flags := flag.new_flag_parser(os.args)

	mut opts := Opts{}

	flags.application(app_name)
	clean := flags.string('clean', `c`, '', 'Cleans input data (PostgreSQL dump)')
	out := flags.string('output', `o`, '', 'Output file for cleaned data')
	length := flags.int('length', `l`, 2, 'Minimum length of the cleaned strings')

	if flags.bool('help', `h`, false, 'Displays usage') {
		println(flags.usage())
		exit(0)
	}

	if clean == '' {
		flags.limit_free_args(2, 2) or {
			println('Please specify input config file')
			exit(0)
		}
		opts.file = flags.remaining_parameters()[1]
	} else {
		flags.limit_free_args(0, 0) or {
			eprintln('Unexpected argument: ${err.msg()}')
			exit(1)
		}
	}

	// data cleaning
	if clean != '' {
		if out == '' {
			eprintln('--clean must be used with --output')
			exit(1)
		}
		mut c := CleanOpts{}
		c.input = clean
		c.output = out
		c.min_length = u8(length)
		opts.clean = c
	} else {
		if out != '' {
			eprintln('--output must be used with --clean')
			exit(1)
		}
	}

	return opts
}
