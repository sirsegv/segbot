import os
import time
import generators
import outputs
import inputdata

const app_name = 'segbot'
const sleep_duration = time.second

// Contains general info about the bot
struct Program {
mut:
	config_name string // config file name
	data SaveData

	post_frequency f32 // post frequency in minutes

	input inputdata.InputData
	history inputdata.InputData

	cw ?string

	generator ?generators.Generator
	outputs []outputs.Output

	opts Opts
}

// Initialises the program and parses the config
fn (mut p Program) start()! {
	p.opts = parse_flags()
	
	if clean := p.opts.clean {
		clean_input_data(clean.input, clean.output, clean.min_length)!
		exit(0)
	}

	p.config_name = os.file_name(p.opts.file).trim_right('.bot')

	p.run_commands(p.opts.file) or {
		eprintln('Error parsing ${p.opts.file}: ${err.msg()}')
		exit(1)
	}

	p.data.load_data(p.data_file(data_filename)) or {
		eprintln('Unable to load save data: ${err.msg()}')
	}

	if p.opts.unique {
		// Initialise history
		p.history = inputdata.InputData{files: [os.join_path(p.config_name, history_filename)], quiet: true}
	}

	// provide output with config name
	for mut o in p.outputs {
		o.init(p.data_file(''))
	}

	println('Starting ${p.config_name}')

	if p.opts.skip_wait {
		p.post()
	}
}

// Sends a post
fn (mut p Program) post() {
	post := p.generate_post()

	// send generated text to outputs
	for mut out in p.outputs {
		out.send(post)
	}

	// save last post
	p.data.last_post = time.utc()
}

// Generates the text for a post
fn (mut p Program) generate_post() outputs.Message {
	output_string := if g := p.generator {
		if p.opts.unique {
			// unique output
			res := g.generate_unique(p.input.get(), p.history.get())
			p.add_history(res)
			res
		} else {
			// standard output
			g.generate(p.input.get())
		}
	} else {
		eprintln('No generator selected')
		exit(1)
	}

	output_message := outputs.Message{output_string, p.cw}

	return output_message
}

// Receives and responds to all replies coming in through the outputs
fn (mut p Program) process_replies() {
	check_time := time.utc()

	for mut output in p.outputs {
		for r in output.get_replies(p.data.last_reply_check) {
			output.reply(outputs.Reply{r.id, r.user, r.visibility, p.generate_post()})
		}
	}

	p.data.last_reply_check = check_time
}

// Shuts down the bot
fn (mut p Program) stop() {
	p.data.save_data(p.data_file(data_filename))
	println('Shutting down...')
	exit(0)
}

// Main function and post loop
fn main() {
	mut p := Program{}
	
	p.start()!

	bind_signals(mut p) or {
		eprintln('Failed to bind terminate signal to exit handler')
	}

	// Main loop
	for {
		// process replies until next post
		for time.utc() <= p.data.last_post.add_seconds(int(p.post_frequency * 60)) {
			time.sleep(sleep_duration)
			p.process_replies()
		}

		p.post()
	}

	p.data.save_data(p.data_file(data_filename))
}

// Let the program gracefully shut down
fn bind_signals(mut p Program) ! {
	os.signal_opt(.term, fn [mut p] (_ os.Signal) { p.stop() })!
	os.signal_opt(.int, fn [mut p] (_ os.Signal) { p.stop() })!
}
