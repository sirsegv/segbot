module generators

interface Generator {
	generate([]string) string
}

pub fn (g Generator) generate_unique(input []string, history []string) string {
	for {
		result := g.generate(input)

		if !history.contains(result) {
			return result
		}
	}

	return ''
}