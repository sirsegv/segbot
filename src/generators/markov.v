module generators

import markov

pub struct MarkovGenerator {
pub:
	n_order u8
}

// Generates a Markov chain from the input data
fn (mg MarkovGenerator) generate(input []string) string {
	mut output_string := ''
	for {
		// Generates N-Gram
		n_gram, beginnings := markov.to_ngram(input, mg.n_order) or {
			eprintln('Error generating markov chain: ${err.msg()}')
			return ''
		}
		// Uses N-Gram to generate new sentence
		output_string = markov.from_ngram(n_gram, beginnings)

		// Ensure generated sentence doesn't exist in input
		if !input.contains(output_string) {
			break
		}
	}
	return output_string
}