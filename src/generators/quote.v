module generators

import rand

pub struct QuoteGenerator {}

// Chooses a source input line and returns it verbatim
fn (qg QuoteGenerator) generate(input []string) string {
	seed := rand.u32() % input.len
	return input[seed]
}