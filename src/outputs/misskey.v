module outputs

import os
import x.json2
import net.http
import math.big
import time

pub struct Misskey {
pub:
	instance string
	api_key string
mut:
	reply_history []string
	reply_history_file string
}

struct MisskeyNote {
	text string
	cw ?string
	reply_id ?string @[json: 'replyId']
	visible_user_ids []string @[json: 'visibleUserIds']
	visibility string = 'public'
}

struct MisskeyReply {
	id string
	user MisskeyUser
	visibility string = 'public'
	text string
}

struct MisskeyUser {
	id string
	username string
}

fn (mut mk Misskey) init(config_name string) {
	mk.reply_history_file = os.join_path(config_name, 'mk_reply_history')

	history_lines := os.read_lines(mk.reply_history_file) or {
		return
	}

	for line in history_lines {
		mk.reply_history << line
	}
}

fn (mk Misskey) send(text Message) {
	note := MisskeyNote{text.text, text.cw, none, [], 'public'}
	note_json := json2.encode(note)

	// Generate request and add headers
	mut req := http.new_request(.post, mk.instance.trim_right('/') + '/api/notes/create', note_json)
	req.add_header(.authorization, 'Bearer ${mk.api_key}')
	req.add_header(.content_type, 'application/json')
	
	// Send request
	response := req.do() or {
		eprintln('Error submitting note: ${err.msg()}')
		return
	}
	if response.status_code != 200 {
		eprintln('HTTP response from note post ${response.status_code}: ${response}')
	}
}

pub fn (mut mk Misskey) get_replies(since time.Time) []Reply {
	mut req := http.new_request(.post, mk.instance.trim_right('/') + '/api/notes/mentions', '{"sinceId":"${gen_aid(since)}"}')
	req.add_header(.authorization, 'Bearer ${mk.api_key}')
	req.add_header(.content_type, 'application/json')
	
	// Send request
	response := req.do() or {
		eprintln('Error requesting replies: ${err.msg()}')
		return []
	}
	if response.status_code != 200 {
		eprintln('HTTP response from notification fetch ${response.status_code}: ${response}')
		return []
	}
	
	mk_replies := json2.decode_array[MisskeyReply](response.body) or {
		eprintln('Error decoding replies JSON: ${err.msg()}')
		return []
	}

	mut replies := []Reply{}

	mut replied := false
	for r in mk_replies {
		if !mk.reply_history.contains(r.id) {
			replied = true
			mk.reply_history << r.id

			replies << Reply{r.id, r.user.username, r.visibility, Message{r.text, none}}
		}
	}

	if replied {
		os.write_file(mk.reply_history_file, mk.reply_history.join('\n')) or {
			eprintln('Unable to save reply history: ${err.msg()}')
			panic(err)
		}
	}

	return replies
}

pub fn (mk Misskey) reply(r Reply) {
	note := MisskeyNote{'@${r.user} ${r.content.text}', r.content.cw, r.id, [r.user], r.visibility}
	note_json := json2.encode(note)

	// Generate request and add headers
	mut req := http.new_request(.post, mk.instance.trim_right('/') + '/api/notes/create', note_json)
	req.add_header(.authorization, 'Bearer ${mk.api_key}')
	req.add_header(.content_type, 'application/json')
	
	// Send request
	response := req.do() or {
		eprintln('Error submitting note: ${err.msg()}')
		return
	}

	if response.status_code != 200 {
		eprintln('HTTP response from reply ${response.status_code}: ${response}')
	}
}

pub fn gen_aid(t time.Time) string {
	fetch_offet := 10000 // fetch notes that are 10 seconds older than last check to keep up with late federations
	time_offset := big.integer_from_i64(t.unix_milli() - 946684800000 - fetch_offet)
	return time_offset.radix_str(36) + '00'
}
