module outputs

import time

pub struct Print {}

fn (mut p Print) init(config_name string) {
	return
}

fn (p Print) send(text Message) {
	println(text.text)
}

fn (p Print) reply(r Reply) {
	println('Replying to ${r.id} from ${r.user}: ${r.content.text}')
}

pub fn (p Print) get_replies(since time.Time) []Reply {
	return []
}