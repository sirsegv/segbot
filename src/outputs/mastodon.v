module outputs

import net.http
import time

pub struct Mastodon {
pub:
	instance string
	api_key string
}

struct MastodonPost {
	status string
}

fn (mut md Mastodon) init(config_name string) {
	return
}

fn (md Mastodon) send(text Message) {
	// Generate request and add headers
	mut req := http.new_request(.post, md.instance.trim_right('/') + '/api/v1/statuses?status=' + text.text, '')
	req.add_header(.authorization, 'Bearer ${md.api_key}')
	req.add_header(.content_type, 'application/json')
	
	// Send request
	response := req.do() or {
		eprintln('Error submitting post: ${err.msg()}')
		return
	}
	if response.status_code != 200 {
		eprintln('HTTP response ${response.status_code}: ${response}')
	}
}

// TODO: Replies not yet implemented for Mastodon
pub fn (md Mastodon) get_replies(since time.Time) []Reply {
	return []
}

pub fn (md Mastodon) reply(r Reply) {
	println('TODO: Replies not yet implemented for Mastodon')
}