module outputs

import time

// Interface for all output methods
interface Output {
mut:
	init(string)
	send(Message)
	reply(Reply)
	get_replies(time.Time) []Reply
}

// Represents a reply (either incoming or outgoing)
pub struct Reply {
pub:
	id string // id of other users reply
	user string // username of other user
	visibility string
	content Message // content of message
}

pub struct Message {
pub:
	text string
	cw ?string
}