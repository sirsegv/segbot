# segbot

**An all purpose program for creating simple bots**

## Description

Segbot is a program for creating bots which generate sentences to post on a regular interval

## How to run

Download a build from [releases](https://codeberg.org/sirsegv/segbot/releases)

write a config file (detailed below) and save it as e.g. `my_cool_bot.bot`

execute segbot with the config file as the argument

```
./segbot my_cool_bot.bot
```

## Config file

A simple config for generating a markov chain and printing to standard output is as follows:

```
name testbot
load source_text.txt
generate markov
delay 0.03
skip_wait
unique
output print
```

## Available commands

Each valid configuration command and their purpose:

`name`: sets the name for the bot to use in its config.

`load`: loads a source file of sentences used to generate an output.

`generate`: specify either `markov` or `quote` to generate either a markov chain or direct quote.

`output`: specify an output location (e.g. `print`, `misskey`, or `mastodon`). may require additional arguments for details such as api keys.

`unique`: ensures the same output is never generated twice. this involves storing each previously generated quote, and for long-running or frequently posting bots may not be desired.

`delay`: how long in minutes to wait before generating another output.

`skip_wait`: debug command to immediately generate another output when run rather than waiting for the delay time to finish.

`cw`: content warning, any following text is used as a content warning/spoiler tag on applicable outputs.

## Supported destinations

Segbot can currently output to the following places:

- standard out (print)
- [Misskey](https://misskey-hub.net/)
- [Mastodon](https://joinmastodon.org/)
